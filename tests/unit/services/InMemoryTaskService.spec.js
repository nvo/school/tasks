// Services
import {
    InMemoryTaskService
} from "@/services/InMemoryTaskService";

// Models
import {
    Task,
    priorities
} from "@/models/Task";

describe("InMemoryTaskService", () => {
    const tasks = [
        new Task("test", false, priorities.HIGH),
        new Task("task")
    ];
    const initialLength = tasks.length;
    let svc;

    beforeEach(() => {
        svc = new InMemoryTaskService();
        InMemoryTaskService.tasks = tasks.slice();
    });

    it("findAll", () => {
        svc.findAll().then(all => expect(all).toEqual(tasks));
    });

    it("find", () => {
        const id = tasks[0].id;
        svc.find(id).then(task => expect(task).toEqual(tasks[id]));
    });

    it("create", () => {
        const task = new Task("new task");
        svc.create(task)
            .then(nTask => {
                svc.findAll()
                    .then(all => expect(all.length).toEqual(initialLength + 1))
                    .then(all => expect(nTask).toEqual(all[all.length - 1]));
            });
    });

    it("edit", () => {
        const id = tasks[0].id;
        const description = "edited";

        svc.find(id)
            .then(task => {
                task.description = description;
                task.random = "some data";

                expect().not.toEqual(task);

                svc.edit(task)
                    .then(updated => expect(svc.find(id)).toEqual(updated));
            });
    });

    it("remove", () => {
        const id = tasks[0].id;

        svc.find(id)
            .then(task => expect(task).toBeDefined());

        svc.remove(id)
            .then(() => {
                svc.find(id)
                    .then(task => expect(task).toBeDefined());

                svc.findAll().then(all => expect(all.length).toEqual(initialLength - 1));
            });
    });
});