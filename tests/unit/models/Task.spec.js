import {
    Task,
    priorities
} from "@/models/Task";
import {
    SubTask
} from "@/models/SubTask";

describe("Task", () => {
    it("creates a new task", () => {
        const description = "description";
        const done = true;
        const priority = priorities.HIGH;
        const creationDate = new Date(2019, 1, 1);
        const image = "image link";
        const position = {
            x: 1,
            y: 1
        };
        const subtasks = [new SubTask("subtask 1", done)];
        const task = new Task(description, done, priority, creationDate, image, position, subtasks);

        validate(task, description, done, priority, creationDate, image, position, subtasks);
    });

    it("creates a new task, no arguments", () => {
        const task = new Task();

        validate(task, "", false, priorities.NORMAL, task.creationDate, "", {
            x: -1,
            y: -1
        }, []);
    });

    it("creates a new task, invalid arguments", () => {
        const description = 1.0;
        const done = "done";
        const priority = true;
        const creationDate = "today";
        const image = 6;
        const position = {
            x: "string",
            y: true
        };
        const subtasks = 3.14;
        const task = new Task(description, done, priority, creationDate, image, position, subtasks);

        validate(task, "", false, priorities.NORMAL, task.creationDate, "", {
            x: -1,
            y: -1
        }, []);
    });
});

function validate(task, description, done, priority, creationDate, image, position, subtasks) {
    expect(task.description).toEqual(description);
    expect(task.done).toBe(done);
    expect(task.priority).toEqual(priority);
    expect(task.creationDate).toEqual(creationDate);
    expect(task.image).toEqual(image);
    expect(task.position).toEqual(position);
    expect(task.subtasks).toEqual(subtasks);
}