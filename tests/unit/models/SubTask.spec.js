import {
    SubTask
} from "@/models/SubTask";

describe("SubTask", () => {
    it("creates a new subtask", () => {
        const description = "description";
        const done = true;
        const subtask = new SubTask(description, done);

        validate(subtask, description, done);
    });

    it("creates a new subtask, no arguments", () => {
        const subtask = new SubTask();

        validate(subtask, "", false);
    });

    it("creates a new subtask, invalid arguments", () => {
        const description = 1.0;
        const done = "done";
        const subtask = new SubTask(description, done);

        validate(subtask, "", false);
    });
});

function validate(subtask, description, done) {
    expect(subtask.description).toEqual(description);
    expect(subtask.done).toBe(done);
}