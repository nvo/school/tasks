import taskServiceMixin from "@/mixins/TaskService";

describe("TaskServiceMixin", () => {
    it("service should exist", () => {
        expect(typeof taskServiceMixin.data === "function").toBe(true);
        const taskService = taskServiceMixin.data().taskService;

        expect(typeof taskService === "object").toBe(true);
        expect(typeof taskService.findAll === "function").toBe(true);
        expect(typeof taskService.find === "function").toBe(true);
        expect(typeof taskService.create === "function").toBe(true);
        expect(typeof taskService.edit === "function").toBe(true);
        expect(typeof taskService.remove === "function").toBe(true);
    });
});