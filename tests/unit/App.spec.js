// Libraries
import Vue from "vue";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import Vuex from "vuex";
import VueCookies from "vue-cookies";

// Components
import App from "@/App";
import TheHeader from "@/components/TheHeader";
import TheFooter from "@/components/TheFooter";
import TheNavBar from "@/components/TheNavBar";

// Utilities
import {
    mount
} from "@vue/test-utils";

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueCookies);

describe("App", () => {
    let state;
    let mutations;
    let store;
    let router;
    let vuetify;
    let wrapper;

    beforeEach(() => {
        state = {
            title: "test"
        };
        mutations = {
            setTitle: () => ""
        };
        store = new Vuex.Store({
            state,
            mutations
        })

        router = new VueRouter();
        vuetify = new Vuetify({
            mocks: {
                $vuetify: {
                    breakpoint: {
                        smAndDown: false
                    }
                }
            }
        });

        wrapper = mount(App, {
            store,
            router,
            vuetify
        });
    });

    it("renders a vue instance", () => {
        expect(wrapper.isVueInstance()).toBe(true);
    });

    it("has a header", () => {
        expect(wrapper.contains(TheHeader)).toBe(true);
    });

    it("has a footer", () => {
        expect(wrapper.contains(TheFooter)).toBe(true);
    });

    it("has a nav bar", () => {
        expect(wrapper.contains(TheNavBar)).toBe(false);
    });
});