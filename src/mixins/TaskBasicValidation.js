const descriptionDefault = "";
const doneDefault = false;

export default {
  data() {
    return {
      description: descriptionDefault,
      done: doneDefault,
      descriptionRules: [v => !!v || "Description is required"],
      doneRules: []
    }
  }
}