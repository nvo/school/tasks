const validDefault = false;

export default {
  data() {
    return {
      valid: validDefault
    }
  },
  watch: {
    valid() {
      this.onValidChanges();
    }
  },
  methods: {
    onValidChanges() {
      this.$emit("check", this.valid);
    }
  }
}