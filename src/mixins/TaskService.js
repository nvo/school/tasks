import {
    RemoteTaskService
} from "@/services/RemoteTaskService";
import {
    InMemoryTaskService
} from "@/services/InMemoryTaskService";

export default {
    data() {
        return {
            taskService: process.env.NODE_ENV === "production" ?
                new InMemoryTaskService() : new RemoteTaskService("http://localhost:3000/tasks")
        }
    }
}