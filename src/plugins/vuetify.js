import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#0892a5',
        secondary: '#222e50',
        accent: '#0ca4a5',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      },

      dark: {
        primary: '#0892a5',
        secondary: '#222e50',
        accent: '#0ca4a5',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      },
    },

    options: {
      customProperties: true
    },
  },
  icons: {
    iconfont: "fa",
  },
});