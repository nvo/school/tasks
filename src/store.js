import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    title: "Tasks"
  },
  mutations: {
    setTitle(state, n) {
      state.title = n;
    }
  },
  actions: {

  }
});