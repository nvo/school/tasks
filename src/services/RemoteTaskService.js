import {
    Task
} from "@/models/Task";

export class RemoteTaskService {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    async findAll() {
        return fetch(this.baseUrl, {
                method: "GET"
            })
            .then(res => res.json())
            .then(res => res.map(x => Object.assign(new Task, x)));
    }

    async find(id) {
        if (!id) throw "No task found with id=" + id;

        const url = this.baseUrl + (this.baseUrl.endsWith("/") ? "" : "/") + id;
        return fetch(url, {
                method: "GET"
            })
            .then(res => res.json())
            .then(res => Object.assign(new Task, res));
    }

    async findBy(callback) {
        this.findAll().then(list => list.find(callback))
            .then(res => Object.assign(new Task, res));
    }

    async create(obj) {
        return fetch(this.baseUrl, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(obj)
            })
            .then(res => res.json())
            .then(res => Object.assign(new Task, res));
    }

    async edit(obj) {
        const url = this.baseUrl + (this.baseUrl.endsWith("/") ? "" : "/") + obj.id;
        return fetch(url, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(obj)
            })
            .then(res => res.json())
            .then(res => Object.assign(new Task, res));
    }

    async remove(id) {
        if (!id) return null;

        const url = this.baseUrl + (this.baseUrl.endsWith("/") ? "" : "/") + id;
        return fetch(url, {
            method: "DELETE"
        });
    }
}