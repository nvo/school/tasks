export class InMemoryTaskService {
    static tasks = []

    constructor() {}

    async findAll() {
        return InMemoryTaskService.tasks;
    }

    async find(id) {
        const task = InMemoryTaskService.tasks.find((t) => {
            return t.id == id
        });

        if (!task) {
            throw "No task found with id=" + id;
        }

        return task;
    }

    async findBy(callback) {
        return InMemoryTaskService.tasks.find(callback);
    }

    async create(obj) {
        const length = InMemoryTaskService.tasks.push(obj);
        return InMemoryTaskService.tasks[length - 1];
    }

    async edit(obj) {
        const found = this.find(obj.id)
            .then(x => {
                for (let key in obj) {
                    if (obj.hasOwnProperty(key) && typeof obj[key] !== 'function' && x.hasOwnProperty(key)) {
                        Reflect.set(x, key, obj[key]);
                    }
                }
                return x;
            });
        return found;
    }

    async remove(id) {
        const index = InMemoryTaskService.tasks.findIndex((t) => {
            return t.id == id
        });
        InMemoryTaskService.tasks.splice(index, 1)
    }
}