const defaults = {
    description: "",
    done: false
};

export class SubTask {
    static id = 0;

    constructor(description, done) {
        this.id = SubTask.id++;
        this.description = description && typeof description === 'string' ? description : defaults.description;
        this.done = done && typeof done === 'boolean' ? done : defaults.done;
    }
}