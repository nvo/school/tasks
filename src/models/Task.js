import {
    SubTask
} from "./SubTask";

export const priorities = {
    LOW: "low",
    NORMAL: "normal",
    HIGH: "high"
}

export const colors = {
    LOW: "success",
    NORMAL: "info",
    HIGH: "error"
}

export function getPriority(index) {
    switch (index) {
        case 0:
            return priorities.LOW;
        case 1:
            return priorities.NORMAL;
        case 2:
            return priorities.HIGH;
    }
}

export function getColor(priority) {
    switch (priority) {
        case priorities.LOW:
            return colors.LOW;
        case priorities.NORMAL:
            return colors.NORMAL;
        case priorities.HIGH:
            return colors.HIGH;
    }
}

const defaults = {
    priority: priorities.NORMAL,
    creationDate: Date.now(),
    image: "",
    position: {
        x: -1,
        y: -1
    },
    subtasks: []
};

export class Task extends SubTask {
    constructor(description, done, priority, creationDate, image, position, subtasks) {
        super(description, done);
        this.priority = priority && typeof priority === "string" ? priority : defaults.priority;
        this.creationDate = creationDate && typeof creationDate.getMonth === "function" ? creationDate : defaults.creationDate;
        this.image = image && typeof image === "string" ? image : defaults.image;
        this.position = position && position.x && typeof position.x === "number" && position.y && typeof position.x === "number" ? position : {
            ...defaults.position
        };
        this.subtasks = subtasks && Array.isArray(subtasks) ? subtasks : [...defaults.subtasks];
    }
}