export default {
    theme: {
        key: "theme",
        values: {
            light: "light",
            dark: "dark"
        }
    }
}