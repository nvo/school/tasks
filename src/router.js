import Vue from "vue";
import Router from "vue-router";
import TaskListView from "@/views/TaskListView";
import TaskDetailView from "@/views/TaskDetailView";

Vue.use(Router);

export default new Router({
  mode: "hash",
  base: process.env.BASE_URL,
  routes: [{
      path: "/",
      name: "tasks",
      component: TaskListView,

      props: (route) => ({
        imageMode: Number(route.query.mode || "0")
      }),

      children: [{
        path: ":id",
        name: "task",
        component: TaskDetailView,
        props: (route) => ({
          id: Number(route.params.id)
        })
      }]
    },

    {
      path: "*",
      component: TaskListView
    }
  ]
});